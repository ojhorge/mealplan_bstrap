const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')
const withPlugins = require('next-compose-plugins')

const SASSConfig = {
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  }
};

const CSSConfig = {
  cssModules: true
};

module.exports = withPlugins([
  [withSass, SASSConfig],
  [withCSS, CSSConfig]
])
