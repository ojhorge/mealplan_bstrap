/* /components/Layout.js */

import React from "react";
import Head from "next/head";
import Link from "next/link";
import { unsetToken } from "../../lib/auth";
import { Container, Nav, NavItem } from "reactstrap";
import defaultPage from "../../hocs/defaultPage";
import Cookie from 'js-cookie';

class Layout extends React.Component {
	constructor(props) {
		super(props);
	}
	static async getInitialProps({ req }) {
		let pageProps = {};
		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}

		return { pageProps, isAuthenticated };
	}
	render() {
		const { isAuthenticated, children } = this.props;
		const title = "Meal Plan";
		return (
			<div>
				<Head>
					<title>{title}</title>
					<meta charSet="utf-8" />
					<meta
						name="viewport"
						content="initial-scale=1.0, width=device-width"
					/>
					<link
						rel="stylesheet"
						href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
						integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
						crossOrigin="anonymous"
					/>
					<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800"
						rel="stylesheet" />
					<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
						rel="stylesheet" />
					<link rel="shortcut icon" href="favicon.ico" />
				</Head>
				<header>
					<Nav className="navbar navbar-dark bg-dark">
						<NavItem>
							<Link href="/">
								<a className="navbar-brand">Home</a>
							</Link>
						</NavItem>
						{isAuthenticated ? (
							<>
								<NavItem className="ml-auto">
									<span style={{ color: "white", marginRight: 30 }}>
										{this.props.loggedUser}
									</span>
								</NavItem>
								<NavItem>
									<Link href="/">
										<a className="logout" onClick={unsetToken}>
											Logout
										</a>
									</Link>
								</NavItem>
							</>
						) : (
							<>
								<NavItem className="ml-auto">
									<Link href="/signin">
										<a className="nav-link">Sign In</a>
									</Link>
								</NavItem>

								<NavItem>
									<Link href="/signup">
										<a className="nav-link"> Sign Up</a>
									</Link>
								</NavItem>
							</>
						)}
					</Nav>
				</header>
				<Container>{children}</Container>
			</div>
		);
	}
}

export default defaultPage(Layout);
