import React from 'react'
import Head from "next/head";
import defaultPage from '../../hocs/defaultPage'

class Header extends React.Component {
  render() {
    const title = "Meal Plan";

    return (
      <>
        <Head>
          <title>{title}</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
            integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
            crossOrigin="anonymous"
          />
          <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800"
            rel="stylesheet" />
          <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
            rel="stylesheet" />
          <link rel="shortcut icon" href="/static/favicon.ico" />
        </Head>
      </>
    )
  }
}

export default defaultPage(Header)
