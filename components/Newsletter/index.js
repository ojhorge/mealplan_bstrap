import ReactDOM from 'react-dom'
import { Form, Col, Button, FormGroup, Input, Label } from 'reactstrap'

export class Newsletter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this);
  }

  handleEmailChange = (e) => {
    this.setState({
      email: e.target.value
    });
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input type="email" name="email" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange} />
          </FormGroup>

          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2}}>
              <Button>Subscribe</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}


export default Newsletter
