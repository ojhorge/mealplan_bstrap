import ReactDOM from 'react-dom'
import { CaloricIntake, MacroIntake } from '../../utils/calculators'
import {
  Col,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
  FormText,
  Progress
} from 'reactstrap'
import { GoalRadio } from './form/goalRadio'
import { JobRadio } from './form/jobRadio'

export class MacroCalculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      age: 0,
      gender: 'male',
      heightOption: "feet",
      heightTens: 0,
      heightUnits: 0,
      weightOption: "pounds",
      weightUnits: 0,
      goal: 'fat-loss',
      job: 'L'
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this);
    this.setState({
      calories: CaloricIntake(this.state),
      macros: MacroIntake(this.state)
    })
  }

  handleInputChange = (e) => {
    const target = e.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    });
  }

  render() {
    const heightOp = this.state.heightOption
    const weightOp = this.state.weightOption
    let heightInputs;
    let weightInputs;
    let macroResults;
    let macroCharts;

    if (heightOp === 'feet') {
      heightInputs = (
        <div>
          <Input type="number" name="heightTens" placeholder="Feet" value={this.state.heightTens} onChange={this.handleInputChange} />
          <Input type="number" name="heightUnits" placeholder="Inches" value={this.state.heightUnits} onChange={this.handleInputChange} />
        </div>
      )
    } else if (heightOp === 'meters') {
      heightInputs = (
        <div>
          <Input type="number" name="heightTens" placeholder="Meter" value={this.state.heightTens} onChange={this.handleInputChange} />
          <Input type="number" name="heightUnits" placeholder="Centimeters" value={this.state.heightUnits} onChange={this.handleInputChange} />
        </div>
      )
    }

    if (weightOp === 'pounds') {
      weightInputs = (
        <Input type="number" name="weightUnits" placeholder="Pounds" value={this.state.weightUnits} onChange={this.handleInputChange} />
      )
    } else if (weightOp === 'kilograms') {
      weightInputs = (
        <Input type="number" name="weightUnits" placeholder="Kilograms" value={this.state.weightUnits} onChange={this.handleInputChange} />
      )
    }

    if (this.state.macros) {
      let carbsPercent = (this.state.macros.carbs * 10) / this.state.macros.carbsMin
      let protonsPercent = (this.state.macros.protons * 10) / this.state.macros.protonsMin
      let fatsPercent = (this.state.macros.fats * 10) / this.state.macros.fatsMin

      macroResults = (
        <p>Carbs: {this.state.macros.carbs} g per day <br />
           Proteins: {this.state.macros.protons} g per day <br />
           Fats: {this.state.macros.fats} g per day</p>
      )

      macroCharts = (
        <div>
          <Progress multi>
            <Progress bar value={carbsPercent}>{this.state.macros.carbs} g of Carbs</Progress>
            <Progress bar color="warning" value={protonsPercent}>{this.state.macros.protons} g of Protein</Progress>
            <Progress bar color="danger" value={fatsPercent}>{this.state.macros.fats} g of Fats</Progress>
          </Progress>
        </div>
      )
    }

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="age">Age</Label>
            <Input name="age" id="age" placeholder="Age" value={this.state.age} onChange={this.handleInputChange} />
          </FormGroup>

          <FormGroup>
            <Label for="gender">Gender</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.gender}>
              <Label check>
                <Input type="radio" name="gender" value="male" />{' '}
                Male
              </Label>
            </FormGroup>
            <FormGroup check onChange={this.handleInputChange} value={this.state.gender}>
              <Label check>
                <Input type="radio" name="gender" value="female" />{' '}
                Female
              </Label>
            </FormGroup>
          </FormGroup>

          <FormGroup>
            <Label for="height">Height</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.heightOption}>
              <Label check>
                <Input type="radio" name="heightOption" value="feet" />{' '}
                Feet
              </Label>
              <Label check>
                <Input type="radio" name="heightOption" value="meters" />{' '}
                Meters
              </Label>
            </FormGroup>
            {heightInputs}
          </FormGroup>

          <FormGroup>
            <Label for="weight">Weight</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.weightOption}>
              <Label check>
                <Input type="radio" name="weightOption" value="pounds" />{' '}
                Pounds
              </Label>
              <Label check>
                <Input type="radio" name="weightOption" value="kilograms" />{' '}
                Kilograms
              </Label>
            </FormGroup>
            <br />
            {weightInputs}

          </FormGroup>

          <FormGroup>
            <Label for="job">Activity Level</Label>
            <JobRadio job={this.state.job} handleInputChange={this.handleInputChange} />
          </FormGroup>

          <FormGroup>
            <Label for="goal">Goal</Label>
            <GoalRadio handleInputChange={this.handleInputChange} goal={this.state.goal} />
          </FormGroup>

          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2 }}>
              <Button>Calculate</Button>
            </Col>
          </FormGroup>
        </Form>
        <div className="macros-result">
          {macroResults}
        </div>
        <div className="macros-charts">
          {macroCharts}
        </div>
      </div>
    )
  }
}


export default MacroCalculator
