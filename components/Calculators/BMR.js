import React from 'react'
import ReactDOM from 'react-dom'
import { BMR } from '../../utils/calculators'
import {
  Col,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
  FormText
} from 'reactstrap'

export class BMRCalculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      age: 0,
      gender: 'male',
      heightOption: 'feet',
      heightTens: 0,
      heightUnits: 0,
      weightOption: 'pounds',
      weightUnits: 0
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    debugger;
    this.setState({
      bmr: BMR(this.state)
    })
  }

  handleInputChange = (e) => {
    const target = e.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  render() {
    const heightOp = this.state.heightOption
    const weightOp = this.state.weightOption
    let heightInputs;
    let weightInputs;

    if (heightOp === 'feet') {
      heightInputs = (
        <div>
          <Input type="number" name="heightTens" placeholder="Feet" value={this.state.heightTens} onChange={this.handleInputChange} />
          <Input type="number" name="heightUnits" placeholder="Inches" value={this.state.heightUnits} onChange={this.handleInputChange} />
        </div>
      )
    } else if (heightOp === 'meters') {
      heightInputs = (
        <div>
          <Input type="number" name="heightTens" placeholder="Meter" value={this.state.heightTens} onChange={this.handleInputChange} />
          <Input type="number" name="heightUnits" placeholder="Centimeters" value={this.state.heightUnits} onChange={this.handleInputChange} />
        </div>
      )
    }

    if (weightOp === 'pounds') {
      weightInputs = (
        <Input type="number" name="weightUnits" placeholder="Pounds" value={this.state.weightUnits} onChange={this.handleInputChange} />
      )
    } else if (weightOp === 'kilograms') {
      weightInputs = (
        <Input type="number" name="weightUnits" placeholder="Kilograms" value={this.state.weightUnits} onChange={this.handleInputChange} />
      )
    }

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="age">Age</Label>
            <Input name="age" id="age" placeholder="Age" value={this.state.age} onChange={this.handleInputChange} />
          </FormGroup>

          <FormGroup>
            <Label for="gender">Gender</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.gender}>
              <Label check>
                <Input type="radio" name="gender" value="male" />{' '}
                Male
              </Label>
            </FormGroup>
            <FormGroup check onChange={this.handleInputChange} value={this.state.gender}>
              <Label check>
                <Input type="radio" name="gender" value="female" />{' '}
                Female
              </Label>
            </FormGroup>
          </FormGroup>

          <FormGroup>
            <Label for="height">Height</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.heightOption}>
              <Label check>
                <Input type="radio" name="heightOption" value="feet" />{' '}
                Feet
              </Label>
              <Label check>
                <Input type="radio" name="heightOption" value="meters" />{' '}
                Meters
              </Label>
            </FormGroup>
            {heightInputs}
          </FormGroup>

          <FormGroup>
            <Label for="weight">Weight</Label>
            <FormGroup check onChange={this.handleInputChange} value={this.state.weightOption}>
              <Label check>
                <Input type="radio" name="weightOption" value="pounds" />{' '}
                Pounds
              </Label>
              <Label check>
                <Input type="radio" name="weightOption" value="kilograms" />{' '}
                Kilograms
              </Label>
            </FormGroup>
            <br />
            {weightInputs}

          </FormGroup>

          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2 }}>
              <Button>Calculate</Button>
            </Col>
          </FormGroup>
        </Form>
        <div className="bmr-result">
          {this.state.bmr ? <p>Your BMR: {this.state.bmr} calories/day</p> : ''}
        </div>
      </div>
    )
  }
}


export default BMRCalculator
