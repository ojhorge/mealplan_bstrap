import { FormGroup, Label, Input } from 'reactstrap'

export class JobRadio extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <FormGroup check onChange={this.props.handleInputChange} value={this.props.job}>
        <Label check>
          <Input type="radio" name="job" value="L" />{' '}
          Lightly active (moderate exercise but sedentary job)
        </Label>
        <Label check>
          <Input type="radio" name="job" value="M" />{' '}
          Moderately active (intense exercise but sedentary job)
        </Label>
        <Label check>
          <Input type="radio" name="job" value="V" />{' '}
          Very active (moderate exercise and active job)
        </Label>
        <Label check>
          <Input type="radio" name="job" value="E" />{' '}
          Extra active (intense exercise and active job)
        </Label>
      </FormGroup>
    )
  }
}

export default JobRadio
