import { FormGroup, Label, Input } from 'reactstrap'

export class GoalRadio extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <FormGroup check onChange={this.props.handleInputChange} value={this.props.goal}>
        <Label check>
          <Input type="radio" name="goal" value="fat-loss" />{' '}
          Fat Loss
        </Label>
        <Label check>
          <Input type="radio" name="goal" value="maintenance" />{' '}
          Maintenance
        </Label>
        <Label check>
          <Input type="radio" name="goal" value="muscle-gains" />{' '}
          Muscle Gains
        </Label>
      </FormGroup>
    )
  }
}

export default GoalRadio
