import { Newsletter } from '../components/Newsletter'

const Subscribe = () => (
  <>
    <h2>Subscribe to Newsletter</h2>
    <div style={{ marginTop: 50 }}>
      <Newsletter />
    </div>
  </>
)

export default Subscribe
