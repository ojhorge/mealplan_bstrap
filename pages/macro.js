import { MacroCalculator } from '../components/Calculators/Macro'

const Macro = () => (
  <>
    <h2>Macro Calculator</h2>
    <div style={{ marginTop: 50 }}>
      <MacroCalculator />
    </div>
  </>
)

export default Macro
