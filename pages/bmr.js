import { BMRCalculator } from '../components/Calculators/BMR'

const BMR = () => (
  <>
    <h2>BMR Calculator</h2>
    <div style={{ marginTop: 50 }}>
      <BMRCalculator />
    </div>
  </>
)

export default BMR
