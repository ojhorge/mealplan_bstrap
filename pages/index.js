/* /pages/index.js */
import React from 'react';
import { Button, Alert } from "reactstrap";
import defaultPage from '../hocs/defaultPage';
import "../static/assets/scss/black-dashboard-react.scss"
// import "../static/assets/css/nucleo-icons.css"

class Index extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Alert color="primary">
					Hello Project is strapi-next with Bootstrap
				</Alert>
				&nbsp; <Button color="primary">Hello from nextjs</Button>
			</div>
		)
	}
}

export default defaultPage(Index)
