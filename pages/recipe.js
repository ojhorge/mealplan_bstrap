import gql from 'graphql-tag'
import { withRouter } from 'next/router'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import {
  Container,
  Row,
  Col,
  Table,
  Badge
} from 'reactstrap'

class Recipe extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      data: { loading, error, recipe },
      router,
      context,
      isAuthenticated
    } = this.props
    console.log(this.props)
    console.log(recipe)
    if (error) return "Error loading recipe"

    if (recipe) {
      return (
        <>
          <Container>
            <h1>{recipe.name}</h1>
            <Row>
              <Col>
                <img src={`http://localhost:1337${recipe.thumbnail.url}`} />
              </Col>
            </Row>
            <Row>
              {recipe.isVegan ? <Col><Badge color="success">Vegan</Badge></Col> : ''}
              {recipe.isVegetarian ? <Col><Badge color="success">Vegetarian</Badge></Col> : ''}
              {recipe.hasNuts ? <Col><Badge color="warning">Nuts</Badge></Col> : ''}
              {recipe.recipetags.map(res => (
                <Col>
                  <Badge color="secondary">{res.name}</Badge>
                </Col>
              ))}
            </Row>
            <Row>
              <Col className="calories">
                Calories: <b>{recipe.calories}</b>
              </Col>
              <Col className="carbohydrates">
                Carbs: <b>{recipe.carbohydrates} g</b>
              </Col>
              <Col className="proteins">
                Proteins: <b>{recipe.proteins} g</b>
              </Col>
              <Col className="fats">
                Fats: <b>{recipe.fats} g</b>
              </Col>
            </Row>
            <Row>
              <Col>
                <p>{recipe.description}</p>
              </Col>
            </Row>
            <Row>
              <h2>Ingredients</h2>
              <Table size="sm" responsive borderless striped>
                <tbody>
                  {recipe.Ingredients.ingredientList.map(res => (
                    <tr>
                      <td align="right">{res.quantity} {res.measurement}</td>
                      <td>{res.ingredient}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Row>
            <Row>
              <h2>Instructions</h2>
              <pre>{recipe.instructions}</pre>
            </Row>

          </Container>
        </>
      )
    }
    return <h1>Loading</h1>
  }
}

const GET_RECIPE = gql`
  query($id: ID!) {
    recipe(id: $id) {
      _id
      name
      description
      thumbnail {
        url
      }
      hasNuts
      isVegan
      isVegetarian
      instructions
      calories
      carbohydrates
      proteins
      fats
      recipetags {
        name
      }
      Ingredients
    }
  }
`;

export default compose(
  withRouter,
  graphql(GET_RECIPE, {
    options: props => {
      return {
        variables: {
          id: props.router.query.id
        }
      };
    },
    props: ({ data }) => ({ data })
  })
)(Recipe);
