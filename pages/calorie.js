import { CalorieCalculator } from '../components/Calculators/Calorie'

const Calorie = () => (
  <>
    <h2>Calorie Calculator</h2>
    <div style={{ marginTop: 50 }}>
      <CalorieCalculator />
    </div>
  </>
)

export default Calorie
