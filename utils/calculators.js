export function BMR (data) {
  let result = 0;
  let height = 0;
  let weight = 0;

  debugger;

  if (data.heightOption === "feet") {
    height = ((parseInt(data.heightTens) * 30.48) + (parseInt(data.heightUnits) * 2.54))
  } else {
    height = (parseInt(data.heightTens) * 100) + parseInt(data.heightUnits)
  }

  if (data.weightOption === "pounds") {
    weight = parseInt(data.weightUnits) * 0.453592
  } else {
    weight = parseInt(data.weightUnits)
  }

  if (data.gender === "male") {
    result = Math.round(66.5 + (weight * 13.75) + (height * 5.003) - (parseInt(data.age) * 6.755))
  } else {
    result = Math.round(655 + (weight * 9.563) + (height * 1.850) - (parseInt(data.age) * 4.676))
  }

  return result;
}

export function MacroIntake (data) {
  let result = 0;
  let height = 0;
  let weight = 0;
  let carbs = 0;
  let carbsMin = 0;
  let protons = 0;
  let protonsMin = 0;
  let fats = 0;
  let fatsMin = 0;

  if (data.heightOption === "feet") {
    height = ((parseInt(data.heightTens) * 30.48) + (parseInt(data.heightUnits) * 2.54))
  } else {
    height = (parseInt(data.heightTens) * 100) + parseInt(data.heightUnits)
  }

  if (data.weightOption === "pounds") {
    weight = parseInt(data.weightUnits) * 0.453592
  } else {
    weight = parseInt(data.weightUnits)
  }

  if (data.gender === "male"){
    result = ((weight * 10) + (height * 6.25) - (parseInt(data.age) * 5) + 5)
  } else {
    result = ((weight * 10) + (height * 6.25) - (parseInt(data.age) * 5) - 161)
  }

  switch (data.job) {
    case "L":
      result = Math.round(result * 1.1)
      break;
    case "M":
      result = Math.round(result * 1.3)
      break;
    case "V":
      result = Math.round(result * 1.5)
      break;
    case "E":
      result = Math.round(result * 1.7)
      break;
  }

  switch (data.goal) {
    case "fat-loss":
      if (result <= 2000) result = Math.round(0.9 * result)
      if (result > 2000) result = Math.round(0.8 * result)
      carbs = Math.round(0.40 * result / 4);
      protons = Math.round(0.40 * result / 4);
      fats = Math.round(0.20 * result / 9);
      carbsMin = Math.round(0.10 * result / 4);
      protonsMin = Math.round(0.10 * result / 4);
      fatsMin = Math.round(0.10 * result / 9);
      break;
    case "maintenance":
      carbs = Math.round(0.45 * result / 4);
      protons = Math.round(0.30 * result / 4);
      fats = Math.round(0.25 * result / 9);
      carbsMin = Math.round(0.10 * result / 4);
      protonsMin = Math.round(0.10 * result / 4);
      fatsMin = Math.round(0.10 * result / 9);
      break;
    case "muscle-gains":
      result += 500
      carbs = Math.round(0.45 * result / 4);
      protons = Math.round(0.30 * result / 4);
      fats = Math.round(0.25 * result / 9);
      carbsMin = Math.round(0.10 * result / 4);
      protonsMin = Math.round(0.10 * result / 4);
      fatsMin = Math.round(0.10 * result / 9);
      break;
  }

  result = {
    carbs: carbs,
    protons: protons,
    fats: fats,
    carbsMin: carbsMin,
    protonsMin: protonsMin,
    fatsMin: fatsMin
  }

  return result;
}

export function CaloricIntake (data) {
  let result = 0;
  let height = 0;
  let weight = 0;

  if (data.heightOption === "feet") {
    height = ((parseInt(data.heightTens) * 30.48) + (parseInt(data.heightUnits) * 2.54))
  } else {
    height = (parseInt(data.heightTens) * 100) + parseInt(data.heightUnits)
  }

  if (data.weightOption === "pounds") {
    weight = parseInt(data.weightUnits) * 0.453592
  } else {
    weight = parseInt(data.weightUnits)
  }

  if (data.gender === "male"){
    result = ((weight * 10) + (height * 6.25) - (parseInt(data.age) * 5) + 5)
  } else {
    result = ((weight * 10) + (height * 6.25) - (parseInt(data.age) * 5) - 161)
  }

  switch (data.job) {
    case "L":
      result = Math.round(result * 1.1)
      break;
    case "M":
      result = Math.round(result * 1.3)
      break;
    case "V":
      result = Math.round(result * 1.5)
      break;
    case "E":
      result = Math.round(result * 1.7)
      break;
  }

  switch (data.goal) {
    case "fat-loss":
      if (result <= 2000) result = Math.round(0.9 * result)
      if (result > 2000) result = Math.round(0.8 * result)
      break;
    case "maintenance":
      break;
    case "muscle-gains":
      result += 500
      break;
  }

  return result;
}
